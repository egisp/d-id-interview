import React, {Component} from 'react';
import styled from 'styled-components';
import {Button, Image} from './components';

const AppContainer = styled.div`
    height: 100vh;
    width: 100vw;
    display: flex;
    flex-wrap: wrap;
`;

const ImageContainer = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 64px;
`;

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            originalImage: require('./resources/blank-image.png'),
            protectedImage: require('./resources/blank-image.png'),
        };
    }

    handleSelectedFile = imageType => event => {
        this.setState({[imageType]: URL.createObjectURL(event.target.files[0])});
    };

    render() {
        return (
            <AppContainer className="App">
                <ImageContainer>
                    <Image url={this.state.originalImage} />
                    <Button>
                        <input
                            type="file"
                            accept="image/*"
                            onChange={this.handleSelectedFile('originalImage')}
                            style={{display: 'none'}}
                        />
                        Upload original photo
                    </Button>
                </ImageContainer>
                <ImageContainer>
                    <Image url={this.state.protectedImage} />
                    <Button>
                        <input
                            type="file"
                            accept="image/*"
                            onChange={this.handleSelectedFile('protectedImage')}
                            style={{display: 'none'}}
                        />
                        Upload protected photo
                    </Button>
                </ImageContainer>
            </AppContainer>
        );
    }
}

export default App;
