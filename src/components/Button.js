import styled from 'styled-components';

export const Button = styled.label`
    background: #2962ff;
    color: white;
    text-decoration: none;
    text-align: center;
    padding: 16px 0;
    width: 220px;
    border-radius: 4px;
    cursor: pointer;
    margin-top: 64px;
`;
