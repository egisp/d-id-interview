import * as React from 'react';

export const Image = ({url}) => (
    <div
        style={{
            flex: 1,
            height: '100%',
            width: '100%',
            minHeight: '160px',
            backgroundImage: `url(${url})`,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
        }}
    />
);
